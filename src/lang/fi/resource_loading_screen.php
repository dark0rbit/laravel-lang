<?php

return [
    'btn_label_start' => 'ALOITUS',
    'btn_label_claim' => 'LUNASTA',
    'btn_label_loading' => 'LATAA',
    'btn_label_loadstarsystem' => 'LATAA TÄHTIJÄRJESTELMÄÄ',
    'login_bonus_panel_title' => 'PÄIVÄN KIRJAUTUMISBONUS',
    'login_bonus_item_headline' => 'Päivä %VALUE%',
    'login_bonus_button_get_premium' => 'Hanki PREMIUM',
    'login_bonus_item_headline_premium' => 'PREMIUM-jäsenyydellä saat myös:',
    'login_bonus_item_premium_buybutton' => 'Osta PREMIUM nyt',
    'login_bonus_calendar_expires' => 'Kalenterin nollaamiseen %VALUE% päivä(ä)',
    'login_bonus_tooltip_info_premiumitem' => 'PREMIUM-jäsenyytesi on oltava voimassa ennen 25. päivän tavallisen palkinnon keräämistä, jotta voit saada premium-tason palkinnon.',
    'login_bonus_tooltip_info_items' => 'Kerää päivittäinen palkintosi. Älä huoli, sillä et menetä edistymistäsi, vaikka sinulta jäisikin yksi päivä väliin.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Sinulla täytyy olla voimassa oleva PREMIUM-jäsenyys, ennen kuin voit kerätä palkinnon.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x %VALUE%',
    'login_time_day_short' => 'pv',
    'login_time_hour_short' => 't',
    'login_time_minute_short' => 'min',
];
