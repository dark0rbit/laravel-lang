<?php

return [
    'btn_label_start' => 'スタート',
    'btn_label_claim' => 'CLAIM',
    'btn_label_loading' => 'ロード中',
    'btn_label_loadstarsystem' => 'スターシステムをロード',
    'login_bonus_panel_title' => 'デイリーログインボーナス',
    'login_bonus_item_headline' => '%VALUE%日目',
    'login_bonus_button_get_premium' => 'プレミアムをゲットする',
    'login_bonus_item_headline_premium' => 'プレミアムだと更に以下を入手できます：',
    'login_bonus_item_premium_buybutton' => 'プレミアムを購入しよう',
    'login_bonus_calendar_expires' => 'カレンダーのリセットまで%VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'プレミアム25日目の報酬を受け取るには、プレミアム会員のメンバーシップを必ず有効にした状態にしてください！',
    'login_bonus_tooltip_info_items' => 'デイリー報酬を獲得しよう。 たとえ１日ミスしてしまっても、カレンダーがリセットされるまでは進行状況は失われません。',
    'login_bonus_tooltip_info_premiumitem_deactivated' => '報酬を受け取るにはプレミアム会員メンバーシップが有効になっていないといけません。',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => '日',
    'login_time_hour_short' => '時間',
    'login_time_minute_short' => '分',
];
