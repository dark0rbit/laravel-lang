<?php

return [
    'btn_label_start' => 'ŠTART',
    'btn_label_claim' => 'ZÍSKAŤ',
    'btn_label_loading' => 'NAHRÁVANIE',
    'btn_label_loadstarsystem' => 'NAHRÁVAM HVIEZDNY SYSTÉM',
    'login_bonus_panel_title' => 'BONUS ZA DENNÉ PRIHLÁSENIE',
    'login_bonus_item_headline' => 'Deň %VALUE%',
    'login_bonus_button_get_premium' => 'Staň sa PREMIUM-členom',
    'login_bonus_item_headline_premium' => 'S PREMIUM-členstvom získaš aj:',
    'login_bonus_item_premium_buybutton' => 'Kúp si teraz PREMIUM-členstvo',
    'login_bonus_calendar_expires' => 'Kalendár sa vynuluje o %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Ak chceš získať odmenu Premium-členstva za 25. deň, MUSÍŠ mať aktívne PREMIUM-členstvo pred prijatím štandardnej odmeny za 25. deň!',
    'login_bonus_tooltip_info_items' => 'Získaj svoju dennú odmenu. Žiadny problém: Ak vynecháš jeden deň, neprídeš o svoj progres až do vynulovania kalendára.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Pred získaním odmeny musíš mať aktívne PREMIUM-členstvo.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
