<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'ERHALTEN',
    'btn_label_loading' => 'LOADING',
    'btn_label_loadstarsystem' => 'LADE STERNENSYSTEM',
    'login_bonus_panel_title' => 'Täglicher Login-Bonus',
    'login_bonus_item_headline' => 'Tag %VALUE%',
    'login_bonus_button_get_premium' => 'Hol Dir PREMIUM',
    'login_bonus_item_headline_premium' => 'Als PREMIUM-User erhältst Du:',
    'login_bonus_item_premium_buybutton' => 'Hol Dir PREMIUM',
    'login_bonus_calendar_expires' => 'Login-Kalender neu in: %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Aktiviere Premium, BEVOR Du die Standard-Belohnung am 25. Tag abholst. Erst dann erhältst Du auch die Premium-Belohnung!',
    'login_bonus_tooltip_info_items' => 'Hol Dir Deine tägliche Belohnung! Keine Panik, wenn Du einen Tag verpasst. Dein Fortschritt bleibt erhalten, bis der Login-Kalender Ende des Monats von vorn beginnt.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Aktiviere Premium, um diese Belohnung zu erhalten.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => '%VALUE%x',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'min',
];
