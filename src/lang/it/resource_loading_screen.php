<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'OTTIENI',
    'btn_label_loading' => 'CARICAMENTO',
    'btn_label_loadstarsystem' => 'CARICA SISTEMA GALASSIE',
    'login_bonus_panel_title' => 'BONUS LOGIN QUOTIDIANO',
    'login_bonus_item_headline' => 'Giorno %VALUE%',
    'login_bonus_button_get_premium' => 'Passa a PREMIUM',
    'login_bonus_item_headline_premium' => 'Con PREMIUM ricevi anche:',
    'login_bonus_item_premium_buybutton' => 'Passa subito a PREMIUM',
    'login_bonus_calendar_expires' => 'Il calendario si resetta tra %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Per ritirare la ricompensa premium del 25° giorno DEVI avere attiva un\'utenza Premium prima di accettare la ricompensa standard del 25° giorno!',
    'login_bonus_tooltip_info_items' => 'Ritira la tua ricompensa quotidiana. Non ti preoccupare: anche se un giorno non fai il login, non perderai i tuoi progressi finché il calendario non si resetta.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Devi avere un\'utenza PREMIUM attiva per poter ritirare la ricompensa.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'g',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
