<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'KREVE',
    'btn_label_loading' => 'LASTER',
    'btn_label_loadstarsystem' => 'LASTER STJERNESYSTEM',
    'login_bonus_panel_title' => 'DAGLIG INNLOGGINGSBONUS',
    'login_bonus_item_headline' => 'Dag %VALUE%',
    'login_bonus_button_get_premium' => 'Få tak i PREMIUM',
    'login_bonus_item_headline_premium' => 'Med PREMIUM får du også:',
    'login_bonus_item_premium_buybutton' => 'Kjøp PREMIUM nå',
    'login_bonus_calendar_expires' => 'Kalenderen nullstilles om %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'For å kreve premiumbelønningen på den 25. dagen, MÅ du ha et aktivt PREMIUM-medlemskap før du aksepterer standardbelønningen på den 25. dagen!',
    'login_bonus_tooltip_info_items' => 'Få din daglige belønning. Ikke noe problem: Du mister ikke fremgangen din om du mister en dag før kalenderen nullstilles.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Du må ha et aktivt PREMIUM-medlemskap for å kreve denne belønningen.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 't',
    'login_time_minute_short' => 'm',
];
