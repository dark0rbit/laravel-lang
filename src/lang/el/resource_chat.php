<?php

return [
    'dochat.chat.kickmsg' => 'Αποσυνδέθηκες προσωρινά από τον %ADMIN!',
    'dochat.chat.banmsg' => 'Αποκλείστηκες από τον %ADMIN!',
    'dochat.chat.kickBySystem' => 'Αποσυνδέθηκες προσωρινά από το σύστημα!',
    'dochat.chat.roomnameToShort' => 'Το όνομα δωματίου είναι μικρό!',
    'dochat.chat.banBySystem' => 'Αποκλείστηκες από το σύστημα!',
    'dochat.chat.wrongCommand' => 'Άγνωστη εντολή!',
    'dochat.chat.noMorePrivateRoomsAllowed' => 'Δεν μπορείς να δημιουργήσεις άλλα δωμάτια!',
    'dochat.chat.kickBySystemBadRoomName' => 'Αποσυνδέθηκες προσωρινά λόγω της επιλογής ονόματος δωματίου!',
    'dochat.chat.bannBySystemBadRoomName' => 'Αποκλείστηκες λόγω της επιλογής ονόματος δωματίου!',
    'dochat.chat.wrongArguments' => 'Λάθος εντολή. Παρακαλώ γράψε /help.',
    'dochat.chat.PrivateRoomNotExist' => 'Το ιδιωτικό δωμάτιο δεν υπάρχει.',
    'dochat.chat.PrivateRoomCreated' => 'Δημιουργία δωματίου με το όνομα %ROOM.',
    'dochat.chat.noSupportSlots' => 'Δεν μπορείς προς το παρόν να υποβάλλεις άλλες ερωτήσεις υποστήριξης, επειδή επεξεργάζονται οι ήδη υποβαλλόμενες ερωτήσεις σου. Περίμενε λίγο μέχρι να απαντηθούν οι μέχρι τώρα ερωτήσεις σου.',
    'dochat.chat.supportMessageAccepted' => 'Η ερώτησή σου στάλθηκε σε έναν από τους συντονιστές. Υπολειπόμενες ερωτήσεις: %COUNT',
    'dochat.chat.yourQuestion' => 'Η ερώτησή σου:',
    'dochat.chat.adminAnswer' => 'Η απάντηση από τον %ADMIN:',
    'dochat.chat.userWhispers' => '%USER συνομιλεί:',
    'dochat.chat.userIgnoresYou' => 'Ο χρήστης %USER σε αγνοεί.',
    'dochat.chat.userNowIgnored' => 'Αγνοείς το χρήστη %USER.',
    'dochat.chat.cannotWhisperYourself' => 'Δεν μπορείς να συνομιλήσεις με τον εαυτό σου.',
    'dochat.chat.cannotIgnoreYourself' => 'Δεν μπορείς να αγνοήσεις τον εαυτό σου.',
    'dochat.chat.youBeingIgnored' => 'Από αυτή τη στιγμή ο χρήστης %USER σε αγνοεί.',
    'dochat.chat.noWhisperMessage' => 'Για να συνομιλήσεις με κάποιον γράψε /w username και το κείμενό σου.',
    'dochat.chat.userNotExistOrOnline' => 'Αυτός ο χρήστης δεν υπάρχει ή δεν είναι online.',
    'dochat.chat.userOutOfRange' => 'Ο χρήστης είναι μεν online, δεν ανήκει όμως στη δικιά σου λίστα φίλων.',
    'dochat.chat.noOneIgnored' => 'Αυτήν την στιγμή δεν αγνοείς κανέναν.',
    'dochat.chat.userNotIgnored' => 'Ο χρήστης %USER δεν έχει αγνοηθεί από σένα μέχρι στιγμής.',
    'dochat.chat.userNoMoreIgnored' => 'Ο χρήστης %USER δεν αγνοείται πλέον από εσένα.',
    'dochat.chat.youNoMoreIgnored' => 'Δεν αγνοείσαι πλέον από τον χρήστη %USER.',
    'dochat.chat.youWhisper' => 'Συνομιλείς με τον %USER:',
    'dochat.chat.notRoomOwner' => 'Δεν μπορείς να κλείσεις αυτό το δωμάτιο. Δεν ανήκει σε σένα.',
    'dochat.chat.userinfo' => 'Είσαι ο χρήστης %USER με το sfsUSerID %SFSUSERID και το dbUserId %DBUSERID. Είσαι συνδεδεμένος με τα δωμάτια %ROOMSCONNECTED',
    'dochat.chat.youInvited' => 'Κάλεσες τον %USER',
    'dochat.chat.roomNotExist' => 'Το δωμάτιο δεν υπάρχει.',
    'dochat.chat.noRoomIds' => 'Δεν μεταφέρθηκαν δωμάτια-IDs στο Server.',
    'dochat.chat.roomIdsWrongFormat' => 'Τα μεταφερόμενα δωμάτια-IDs έχουν λάθος φόρμα.',
    'dochat.chat.noRoomsFound' => 'Δεν βρέθηκαν δωμάτια.',
    'dochat.chat.inviteMessage' => '%CREATOR σε κάλεσε στο δωμάτιο %ROOM',
    'dochat.chat.inviteMessageAccept' => 'Αποδοχή πρόσκλησης',
    'dochat.chat.inviteErrorNotYourRoom' => 'Δεν μπορείς να καλέσεις κάποιον σ΄αυτό το δωμάτιο επειδή δεν σου ανήκει. Πήγαινε πρώτα στο δωμάτιό σου πριν καλέσεις κάποιον.',
    'dochat.chat.cannotJoinForeignRooms' => 'Δεν μπορείς να εισχωρήσεις σε ιδιωτικά δωμάτια στα οποία δεν έχεις λάβει πρόσκληση.',
    'dochat.chat.userAlreadyInRoom' => 'Ο %USER βρίσκεται ήδη σ΄ αυτό το δωμάτιο.',
    'dochat.chat.cannotInviteMods' => 'Δεν μπορείς να καλέσεις τους συντονιστές.',
    'dochat.chat.inviteErrorUserNotFound' => 'Ο χρήστης που κάλεσες δεν βρέθηκε.',
    'dochat.chat.cannotInviteYourself' => 'Δεν μπορείς να καλέσεις τον εαυτό σου.',
    'dochat.chat.reconnect' => 'Επανασύνδεση σε %SECONDS δευτερόλεπτα...',
    'dochat.chat.connecting' => 'Σύνδεση...',
    'dochat.chat.connected' => 'Επιτυχής σύνδεση.',
    'dochat.chat.noConnection' => 'Καμία σύνδεση με το ChatServer.',
    'dochat.chat.userEnterRoom' => 'Ο %USER εισχωρεί στο δωμάτιο',
    'dochat.chat.userLeaveRoom' => 'Ο %USER αποχωρεί από το δωμάτιο',
    'dochat.chat.connectionLost' => 'Η σύνδεση με το server διακόπηκε.',
    'dochat.chat.privateRoomAlreadyExist' => 'Το δωμάτιο υπάρχει ήδη!',
    'dochat.chat.userInvited' => 'Ο χρήστης %USER έλαβε πρόσκληση στο δωμάτιο %ROOM.',
    'dochat.chat.refuseInviteMessage' => 'Ο %USER αρνήθηκε την πρόσκλησή σου.',
    'dochat.chat.inviteRefused' => 'Αρνήθηκες την πρόσκληση.',
    'dochat.chat.privateRoomDeleted' => 'Το δωμάτιο %ROOM διαγράφηκε.',
    'dochat.chat.roomsAvailable' => 'Διαθέσιμα δωμάτια:',
    'dochat.chat.room' => 'Δωμάτιο',
    'dochat.chat.owner' => 'Κάτοχος',
    'dochat.chat.userCount' => 'Χρήστης στο δωμάτιο',
    'dochat.chat.roomId' => 'Δωμάτιο-ID',
    'dochat.chat.userInRoom' => 'Χρήστης στο δωμάτιο:',
    'dochat.chat.emailFound' => 'Δεν επιτρέπεται η ανακοίνωση e-mail διευθύνσεων.',
    'dochat.chat.support' => 'Υποστήριξη',
    'dochat.chat.globalChannel' => 'Παγκόσμιος',
    'dochat.chat.companyChannel' => 'Εταιρεία',
    'dochat.chat.spamWarning' => 'Τα Spam δεν είναι εδώ επιθυμητά!',
    'dochat.chat.messageTooLong' => 'Το εισαγμένο μήνυμα είναι πολύ μεγάλο!',
    'dochat.chat.fastTyping' => 'Γράφεις πολύ γρήγορα!',
    'dochat.chat.noCaps' => 'Παρακαλώ γράφε με μικρά γράμματα!',
    'dochat.chat.floodWarning' => 'Το flooding απαγορεύεται!',
    'dochat.chat.htmlWarning' => 'Το HTML δεν επιτρέπεται!',
    'dochat.loginerror.banned' => 'Αποκλείστηκες από την συνομιλία. Μπορείς να κάνεις επανασύνδεση στις %ENDDATE, ώρα %ENDTIME.',
    'dochat.loginerror.bannedForever' => 'Αποκλείστηκες για άγνωστο χρονικό διάστημα.',
    'dochat.loginerror.minutes' => 'Λεπτό(ά)',
    'dochat.loginerror.hours' => 'Ώρα(ες)',
    'dochat.loginerror.days' => 'Μέρα(ες)',
    'dochat.options.textsize' => 'Μέγεθος κειμένου',
    'dochat.options.smoothanimations' => 'Κινούµενες εικόνες',
    'dochat.options.joinpartmessages' => 'Αναφορές δωματίου',
    'dochat.chat.joinRoom' => 'Καλώς ήρθες στο chat δωμάτιο %ROOM',
    'dochat.help.create' => 'δημιουργεί ένα δωμάτιο',
    'dochat.help.close' => 'κλείνει ένα δωμάτιο',
    'dochat.help.invite' => 'καλεί ένα χρήστη στο τρέχων δωμάτιο',
    'dochat.help.ignore' => 'αγνοεί έναν χρήστη',
    'dochat.help.allow' => 'άδεια ομιλίας σε μέχρι στιγμής αγνοημένο χρήστη',
    'dochat.help.users' => 'καταγράφει όλους τους χρήστες στο τρέχων δωμάτιο',
    'dochat.help.rooms' => 'δείχνει μια λίστα όλων των διαθέσιμων δωματίων',
    'dochat.help.whisper' => 'πες κάτι σ΄ένα χρήστη',
    'dochat.chat.wrongVersion' => 'Δεν χρησιμοποιείς την πιο πρόσφατη έκδοση του chat client. Παρακαλώ διάγραψε την προσωρινή μνήμη (cashe) του browser και ενημέρωσε το παιχνίδι.',
    'dochat.chat.administrator' => 'Διαχειριστής',
    'dochat.chat.moderatorsInRoom' => 'Συντονιστές στο δωμάτιο:',
    'dochat.help.leave' => 'Εγκατάλειψη δωματίου',
    'dochat.chat.roomnameTooLong' => 'Το όνομα δωματίου είναι πολύ μεγάλο.',
    'dochat.chat.cannotLeaveRaum' => 'Δεν μπορείς να εγκαταλείψεις αυτό το δωμάτιο.',
    'dochat.chat.system' => 'Σύστημα',
    'dochat.chat.fastReg' => 'Στο chat επιτρέπεται να γράψεις αφότου κάνεις μια πλήρη εγγραφή.',
    'dochat.chat.ttip_reconnect' => 'Επανασύνδεση στον server του chat. Θα σβηστεί όλο το ιστορικό του chat σου!',
    'dochat.help.report' => 'Open Report interface.',
    'error_timedout' => 'Η σύνδεση έληξε.',
    'error_rightscheck' => 'Δεν έχεις άδεια για αυτή την ενέργεια.',
    'error_couldnotloginwithoutcredentials' => 'Χρειάζεσαι διαπιστευτήρια για να συνδεθείς.',
    'error_couldnotloginwithwrongcredentials' => 'Λάθος διαπιστευτήρια.',
    'error_noserver' => 'Δεν υπάρχει διαθέσιμος server συνομιλιών.',
    'error_internalerror' => 'Παρουσιάστηκε εσωτερικό σφάλμα.',
    'error_parsefailed' => 'Δεν ήταν δυνατή η ανάλυση του μηνύματος.',
    'error_custom' => 'Παρουσιάστηκε ένα μη αναμενόμενο σφάλμα.',
    'reportview_message_info' => 'Παρακαλώ επίλεξε το μήνυμα που θέλεις να αναφέρεις:',
    'reportview_label_reason' => 'Αιτία:',
    'reason_spam' => 'Spam',
    'reason_spam_details' => '<b><u>Ανεπιθύμητη επικοινωνία πληροφοριών θεωρείται:</u></b><ul><li>το να γεμίζει το chat με τυχαία μηνύματα</li><li>το να επαναλαμβάνεται συνεχώς το ίδιο μήνυμα</li></ul><br><br><b><u>Ανεπιθύμητη επικοινωνία πληροφοριών ΔΕ θεωρείται:</u></b><ul><li>κάποια έντονη συζήτηση</li></ul>',
    'reason_bad_name' => 'Ακατάλληλο όνομα παίκτη ή συμμαχίας',
    'reason_bad_name_details' => '<b><u>Ακατάλληλο όνομα παίκτη ή συμμαχίας θεωρείται:</u></b><ul><li>Οποιοδήποτε όνομα παίκτη/ομάδας/συμμαχίας που είναι άσεμνο ή προσβλητικό</li><br><br></ul><b><u>Ακατάλληλο όνομα παίκτη ή συμμαχίας ΔΕ θεωρείται:</u></b><ul><li>Η χρήση ειδικών χαρακτήρων, ξένης γλώσσας ή ASCII τέχνης</li></ul>',
    'reason_abusive_chat' => 'Άσεμνο Chat',
    'reason_abusive_chat_details' => '<b><u>Άσεμνο Chat θεωρείται:</u></b><ul><li>η χρήση του chat για διαφήμιση, χυδαία μηνύματα, παρενόχληση, ρατσιστικές, πολιτικές ή θρησκευτικές αντιπαραθέσεις</li></ul><br><br><b><u>Άσεμνο Chat ΔΕ θεωρείται:</u></b><ul><li>αστεία, ιδιωτικές συζητήσεις, ανεπιθύμητη επικοινωνία πληροφοριών, υποδείξεις για τη συμπεριφορά σου</li></ul>',
    'chat_confirm_report' => 'Θέλεις σίγουρα να αναφέρεις τον/την %USERNAME% για %REASON%;',
    'chat_confirm_statement' => 'Σε ευχαριστούμε πολύ! Λάβαμε την αναφορά σου και οι διαχειριστές της κοινότητας θα διερευνήσουν το θέμα. Λυπούμαστε για την όποια ταλαιπωρία.',
    'achat_ban_user' => 'Ο Χρήστης %username% έχει αποκλειστεί από το σύστημα.',
];
