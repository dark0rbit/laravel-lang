<?php

return [
    'btn_label_start' => 'ΕΚΚΙΝΗΣΗ',
    'btn_label_claim' => 'ΔΙΕΚΔΙΚΗΣΕ',
    'btn_label_loading' => 'ΦΟΡΤΩΣΗ',
    'btn_label_loadstarsystem' => 'ΦΟΡΤΩΣΗ ΑΣΤΡΙΚΟΥ ΣΥΣΤΗΜΑΤΟΣ',
    'login_bonus_panel_title' => 'ΗΜΕΡΗΣΙΟ LOGIN BONUS',
    'login_bonus_item_headline' => 'Ημέρα %VALUE%',
    'login_bonus_button_get_premium' => 'Γίνε PREMIUM',
    'login_bonus_item_headline_premium' => 'Με την PREMIUM συνδρομή παίρνεις επίσης:',
    'login_bonus_item_premium_buybutton' => 'Αγόρασε PREMIUM συνδρομή τώρα',
    'login_bonus_calendar_expires' => 'Επαναφορά ημερολογίου σε %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Για να πάρεις την Premium ανταμοιβή 25 ημερών ΠΡΕΠΕΙ να ενεργοποιήσεις την PREMIUM συνδρομή προτού αποδεχθείς την κλασική ανταμοιβή 25 ημερών.',
    'login_bonus_tooltip_info_items' => 'Πάρε την ημερήσια ανταμοιβή σου! Μην ανησυχείς: Αν χάσεις μια μέρα δεν χάνεις την πρόοδό σου μέχρι να γίνει επαναφορά του ημερολογίου.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Πρέπει να έχεις ενεργή PREMIUM συνδρομή για να διεκδικήσεις την ανταμοιβή.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'η',
    'login_time_hour_short' => 'ω',
    'login_time_minute_short' => 'λ',
];
