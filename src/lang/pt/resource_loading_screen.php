<?php

return [
    'btn_label_start' => 'INÍCIO',
    'btn_label_claim' => 'OBTER',
    'btn_label_loading' => 'CARREGANDO',
    'btn_label_loadstarsystem' => 'CARREGANDO SISTEMA ESTRELAR',
    'login_bonus_panel_title' => 'BÓNUS DE LOGIN DIÁRIO',
    'login_bonus_item_headline' => 'Dia %VALUE%',
    'login_bonus_button_get_premium' => 'Obtém PREMIUM',
    'login_bonus_item_headline_premium' => 'Com PREMIUM tambéms recebes:',
    'login_bonus_item_premium_buybutton' => 'Compra PREMIUM agora',
    'login_bonus_calendar_expires' => 'O calendário reinicia em %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Para obteres a recompensa Premium do 25º dia TENS de ativar a afiliação PREMIUM antes de aceitares a recompensa padrão do 25º dia.',
    'login_bonus_tooltip_info_items' => 'Obtém a tua recompensa diária. Não te preocupes: Se perderes um dia não perderás o teu progresso até que o calendário reinicie.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Tens de ter uma afiliação PREMIUM ativa para que possas receber a recompensa.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
