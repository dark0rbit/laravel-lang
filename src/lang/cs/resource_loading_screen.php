<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'ZÍSKAT',
    'btn_label_loading' => 'NAHRÁVÁNÍ',
    'btn_label_loadstarsystem' => 'NAHRÁT HVĚZDNÝ SYSTÉM',
    'login_bonus_panel_title' => 'DENNÍ BONUS ZA PŘIHLÁŠENÍ',
    'login_bonus_item_headline' => 'Den – %VALUE%',
    'login_bonus_button_get_premium' => 'Pořiďte si PRÉMIOVÉ členství',
    'login_bonus_item_headline_premium' => 'PRÉMIOVÉ členství vám navíc zajistí:',
    'login_bonus_item_premium_buybutton' => 'Zakupte si PRÉMIOVÉ členství',
    'login_bonus_calendar_expires' => 'Kalendář se resetuje za %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Pokud chcete získat Prémiovou odměnu za 25. den, MUSÍTE mít před přijetím standardní odměny za 25. den aktivní PRÉMIOVÉ členství.',
    'login_bonus_tooltip_info_items' => 'Získejte denní odměnu! Žádný strach: Pokud vynecháte některý den, neztratíte svůj pokrok, dokud se kalendář neresetuje.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Před přijetím odměny musíte mít aktivní PRÉMIOVÉ členství.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
