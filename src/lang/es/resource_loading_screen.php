<?php

return [
    'btn_label_start' => 'INICIO',
    'btn_label_claim' => 'RECLAMAR',
    'btn_label_loading' => 'CARGANDO',
    'btn_label_loadstarsystem' => 'CARGANDO SISTEMA ESTELAR',
    'login_bonus_panel_title' => 'BONO DE CONEXIÓN DIARIO',
    'login_bonus_item_headline' => 'Día %VALUE%',
    'login_bonus_button_get_premium' => 'Consigue una cuenta PREMIUM',
    'login_bonus_item_headline_premium' => 'Con la cuenta PREMIUM también te llevas:',
    'login_bonus_item_premium_buybutton' => 'Comprar PREMIUM ahora',
    'login_bonus_calendar_expires' => 'El calendario se reinicia en %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => '¡Para llevarte el regalo Premium del 25º día de recompensas, debes tener la cuenta PREMIUM activa antes de aceptar la recompensa habitual del día!',
    'login_bonus_tooltip_info_items' => 'Consigue tu recompensa diaria. ¡No te preocupes! Si no entras un día, no perderás tu progreso hasta que se reinicie el calendario.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Debes tener la cuenta PREMIUM activa antes de reclamar la recompensa.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
