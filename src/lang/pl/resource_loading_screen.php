<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'ODBIERZ',
    'btn_label_loading' => 'ŁADOWANIE',
    'btn_label_loadstarsystem' => 'ŁADUJĘ GWIEZDNY SYSTEM',
    'login_bonus_panel_title' => 'BONUS ZA CODZIENNE LOGOWANIE',
    'login_bonus_item_headline' => 'Dzień %VALUE%',
    'login_bonus_button_get_premium' => 'Zdobądź PREMIUM',
    'login_bonus_item_headline_premium' => 'Dzięki PREMIUM zdobywasz także:',
    'login_bonus_item_premium_buybutton' => 'Kup PREMIUM',
    'login_bonus_calendar_expires' => 'Kalendarz zostanie wyzerowany za %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Aby zdobyć nagrodę Premium, MUSISZ posiadać aktywne konto PREMIUM przed zaakceptowaniem standardowej nagrody za 25 logowań!',
    'login_bonus_tooltip_info_items' => 'Zdobądź swoją nagrodę dnia. Nie martw się! Jeżeli przegapisz dzień, nie stracisz swoich postępów aż do momentu wyzerowania kalendarza.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Aby odebrać tę nagrodę, musisz posiadać aktywne konto PREMIUM.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x %VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
