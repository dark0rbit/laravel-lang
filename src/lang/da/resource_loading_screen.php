<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'HENT',
    'btn_label_loading' => 'LOADING',
    'btn_label_loadstarsystem' => 'INDLÆS STJERNESYSTEM',
    'login_bonus_panel_title' => 'DAGLIG LOGIN BONUS',
    'login_bonus_item_headline' => 'Dag %VALUE%',
    'login_bonus_button_get_premium' => 'Skaf PREMIUM',
    'login_bonus_item_headline_premium' => 'Med PREMIUM får du også:',
    'login_bonus_item_premium_buybutton' => 'Køb PREMIUM nu:',
    'login_bonus_calendar_expires' => 'Kalenderen nulstilles om %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'For at få Premium-præmien ved login den 25. dag, så SKAL du have aktiveret dit PREMIUM medlemskab, INDEN du accepterer den 25. dags præmie!',
    'login_bonus_tooltip_info_items' => 'Få din daglige præmie. Intet problem: Hvis du springer en dag over, mister du ikke dit fremskridt indtil kalenderen nulstilles.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Du skal have et aktivt PREMIUM medlemskab, før du kan kræve denne præmie.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
