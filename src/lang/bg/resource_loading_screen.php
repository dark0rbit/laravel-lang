<?php

return [
    'btn_label_start' => 'СТАРТ',
    'btn_label_claim' => 'ВЗЕМИ',
    'btn_label_loading' => 'ЗАРЕЖДАНЕ',
    'btn_label_loadstarsystem' => 'ЗВЕЗДНАТА СИСТЕМА СЕ ЗАРЕЖДА',
    'login_bonus_panel_title' => 'БОНУС ЗА ЕЖЕДНЕВНО ВЛИЗАНЕ В ИГРАТА',
    'login_bonus_item_headline' => 'Ден %VALUE%',
    'login_bonus_button_get_premium' => 'Вземи ПРЕМИУМ',
    'login_bonus_item_headline_premium' => 'С премимум също така ще получиш:',
    'login_bonus_item_premium_buybutton' => 'Купи ПРЕМИУМ сега',
    'login_bonus_calendar_expires' => 'Календарът ще се нулира след %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'За да получиш премиум наградата на 25-ия ден, ТРЯБВА да имаш активирано премиум членство, преди да приемеш обичайната награда на 25-ия ден!',
    'login_bonus_tooltip_info_items' => 'Вземи наградата си за деня. Не се безпокой: Дори и да пропуснеш ден, няма да изгубиш напредъка си в играта, докато календарът не започне отчитането от начало.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'За да си получиш наградата, трябва да имаш активно ПРЕМИУМ членство.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
