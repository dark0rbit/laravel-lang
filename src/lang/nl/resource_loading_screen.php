<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'CLAIMEN',
    'btn_label_loading' => 'LADEN',
    'btn_label_loadstarsystem' => 'LAAD HET STERRENSYSTEEM',
    'login_bonus_panel_title' => 'DAGELIJKSE INLOGBONUS',
    'login_bonus_item_headline' => 'Dag %VALUE%',
    'login_bonus_button_get_premium' => 'Bemachtig PREMIUM',
    'login_bonus_item_headline_premium' => 'Met PREMIUM krijg je ook:',
    'login_bonus_item_premium_buybutton' => 'Koop nu PREMIUM',
    'login_bonus_calendar_expires' => 'Kalender wordt gereset over %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Om de 25ste dagbeloning voor Premium te claimen, MOET je een actief PREMIUM-lidmaatschap hebben voordat je de beloning van de 25ste dag ophaalt!',
    'login_bonus_tooltip_info_items' => 'Haal je dagelijkse beloning op.  Geen zorgen: als je een dag mist verlies je je voortgang niet, totdat de kalender wordt gereset.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Je moet een actief PREMIUM-lidmaatschap hebben voordat je de beloning kunt claimen.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'u',
    'login_time_minute_short' => 'm',
];
