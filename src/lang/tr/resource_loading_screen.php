<?php

return [
    'btn_label_start' => 'BAŞLA',
    'btn_label_claim' => 'AL',
    'btn_label_loading' => 'YÜKLENİYOR',
    'btn_label_loadstarsystem' => 'YILDIZ SİSTEMİNİ YÜKLE',
    'login_bonus_panel_title' => 'GÜNLÜK GİRİŞ BONUSU',
    'login_bonus_item_headline' => 'Gün %VALUE%',
    'login_bonus_button_get_premium' => 'PREMIUM Al',
    'login_bonus_item_headline_premium' => 'PREMIUM ile şunlara sahip olacaksın:',
    'login_bonus_item_premium_buybutton' => 'Hemen PREMIUM al',
    'login_bonus_calendar_expires' => 'Takvim %VALUE% içerisinde sıfırlanacak',
    'login_bonus_tooltip_info_premiumitem' => 'Premium 25. Gün ödülünü talep etmek için standart 25. gün ödülünü kabul etmeden önce aktif PREMIUM Üyeliğe sahip OLMALISIN.',
    'login_bonus_tooltip_info_items' => 'Günlük ödülünü al. Merak etme: Bir günü kaçırsan bile, takvim sıfırlanıncaya kadar ilerlemelerini kaybetmeyeceksin.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Ödülü alablimek için aktif PREMIUM üyeliğe sahip olmalısın.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'X%VALUE%',
    'login_time_day_short' => 'g',
    'login_time_hour_short' => 's',
    'login_time_minute_short' => 'd',
];
