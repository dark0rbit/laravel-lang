<?php

return [
    'dochat.chat.kickmsg' => '%ADMIN tarafından çıkarıldın!',
    'dochat.chat.banmsg' => '%ADMIN tarafından belirli bir süre yasaklandın!',
    'dochat.chat.kickBySystem' => 'Sistem tarafından çıkarıldın!',
    'dochat.chat.roomnameToShort' => 'Oda ismi çok kısa!',
    'dochat.chat.banBySystem' => 'Sistem tarafında belirli bir süre yasaklandın!',
    'dochat.chat.wrongCommand' => 'Emir tanınmadı!',
    'dochat.chat.noMorePrivateRoomsAllowed' => 'Daha fazla oda kuramazsın!',
    'dochat.chat.kickBySystemBadRoomName' => 'Oda isminde ki seçimin yüzünden, çıkarıldın!',
    'dochat.chat.bannBySystemBadRoomName' => 'Oda isminde ki seçimin yüzünden, belirli bir süre yasaklandın!',
    'dochat.chat.wrongArguments' => 'Emir hatalı. Lütfen /yardım yaz.',
    'dochat.chat.PrivateRoomNotExist' => 'Özel oda bulunmamaktadır.',
    'dochat.chat.PrivateRoomCreated' => '%ROOM isimli oda oluşturuldu.',
    'dochat.chat.noSupportSlots' => 'Supportta daha fazla soru soramazsın, çünkü öncekiller daha işlemde. Lütfen diğer soruların cevaplanıncaya kadar bekle.',
    'dochat.chat.supportMessageAccepted' => 'Sorduğun soru, moderatöre iletildi. Geri kalan soru: %COUNT',
    'dochat.chat.yourQuestion' => 'Senin sorun:',
    'dochat.chat.adminAnswer' => '%ADMIN\'nin cevabı:',
    'dochat.chat.userWhispers' => '%USER gizli konuşuyor:',
    'dochat.chat.userIgnoresYou' => '%USER oyuncu seni görmemezlikten geldi.',
    'dochat.chat.userNowIgnored' => 'Oyuncu %USER\'i görmemezlikten geldin.',
    'dochat.chat.cannotWhisperYourself' => 'Kendi kendine gizli konuşamazsın.',
    'dochat.chat.cannotIgnoreYourself' => 'Kendini görmemezlikten gelemezsin.',
    'dochat.chat.youBeingIgnored' => '%USER tarafından görmemezlikten gelineceksin.',
    'dochat.chat.noWhisperMessage' => 'Birinle gizli konuşmak için, sadece /w nik ismini yazına yaz.',
    'dochat.chat.userNotExistOrOnline' => 'Oyuncu yok veya online değil.',
    'dochat.chat.userOutOfRange' => 'Oyuncu online, ama senin Buddy listene dahil değil.',
    'dochat.chat.noOneIgnored' => 'Şu anda hiç kimseyi görmemezlikten gelmiyorsun.',
    'dochat.chat.userNotIgnored' => 'Şimdiye kadar %USER oyuncusunu görmemezlikten gelmedin.',
    'dochat.chat.userNoMoreIgnored' => '%USER oyuncusunu artık görmemezlikten gelmiyorsun.',
    'dochat.chat.youNoMoreIgnored' => 'Oyuncu %USER seni görmemezliği kaldırdı.',
    'dochat.chat.youWhisper' => '%USER gizli konuşuyorsun:',
    'dochat.chat.notRoomOwner' => 'Sen bu odayı kapatamazsın. Bu oda sana ait değil.',
    'dochat.chat.userinfo' => 'Senin adın %USER, sfsUserId ise %SFSUSERID ve dbUserId\'n %DBUSERID. %ROOMSCONNECTED odaları ile bağlantın var.',
    'dochat.chat.youInvited' => '%USER davet ettin',
    'dochat.chat.roomNotExist' => 'Böyle bir oda yok.',
    'dochat.chat.noRoomIds' => 'Oda-İD\'ler sunucuya iletilmedi.',
    'dochat.chat.roomIdsWrongFormat' => 'İletilen Oda-İD\'lerin yanlış formatı var.',
    'dochat.chat.noRoomsFound' => 'Oda bulunamadı.',
    'dochat.chat.inviteMessage' => '%CREATOR tarafında, %ROOM odasına davet edildin.',
    'dochat.chat.inviteMessageAccept' => 'Daveti kabul et',
    'dochat.chat.inviteErrorNotYourRoom' => 'Bu odada kimseyi davet edemezsin. çünkü oda sana ait değil. Önce kendi odana geç ve birini davet et.',
    'dochat.chat.cannotJoinForeignRooms' => 'Davet edilmediğin özel odalara giremezsin.',
    'dochat.chat.userAlreadyInRoom' => '%USER zaten odanın içinde.',
    'dochat.chat.cannotInviteMods' => 'Moderatör davet edemezsin.',
    'dochat.chat.inviteErrorUserNotFound' => 'Davet edilen üye bulunamadı.',
    'dochat.chat.cannotInviteYourself' => 'Kendi kendini davet edemezsin.',
    'dochat.chat.reconnect' => '%SECONDS saniyede tekrar bağlancaksın...',
    'dochat.chat.connecting' => 'Bağlanıyorsun...',
    'dochat.chat.connected' => 'Bağlantı başarılı.',
    'dochat.chat.noConnection' => 'Chat sunucuya bağlantı yok.',
    'dochat.chat.userEnterRoom' => '%USER odaya girdi.',
    'dochat.chat.userLeaveRoom' => '%USER odadan çıktı',
    'dochat.chat.connectionLost' => 'Sunucuya bağlantı koptu.',
    'dochat.chat.privateRoomAlreadyExist' => 'Bu oda zaten var!',
    'dochat.chat.userInvited' => '%USER %ROOM odasına davet edildi.',
    'dochat.chat.refuseInviteMessage' => '%USER davetini kabul etmedi.',
    'dochat.chat.inviteRefused' => 'Daveti kabul etmedin.',
    'dochat.chat.privateRoomDeleted' => '%ROOM isimli oda silindi.',
    'dochat.chat.roomsAvailable' => 'Mevcut Odalar:',
    'dochat.chat.room' => 'Oda',
    'dochat.chat.owner' => 'Sahibi',
    'dochat.chat.userCount' => 'Oyuncu odada',
    'dochat.chat.roomId' => 'Oda-İD',
    'dochat.chat.userInRoom' => 'Üye odada:',
    'dochat.chat.emailFound' => 'E-Mail adresleri açıklamak yasaktır.',
    'dochat.chat.support' => 'Support',
    'dochat.chat.globalChannel' => 'Global',
    'dochat.chat.companyChannel' => 'Şirket',
    'dochat.chat.spamWarning' => 'Spam burada istenmemektedir!',
    'dochat.chat.messageTooLong' => 'Yazdığın haber çok uzun!',
    'dochat.chat.fastTyping' => 'Çok hızlı yazıyorsun!',
    'dochat.chat.noCaps' => 'Lütfen küçük yazın!',
    'dochat.chat.floodWarning' => 'Taşkınlık yasak!',
    'dochat.chat.htmlWarning' => 'HTML yasak!',
    'dochat.loginerror.banned' => 'Sohbetten banlandın. Tekrar %ENDDATE saat %ENDTIME zamanında giriş yapabilirsin.',
    'dochat.loginerror.bannedForever' => 'Bilinmeyen bir zamana kadar, yasaklandın.',
    'dochat.loginerror.minutes' => 'Dakika(lar)',
    'dochat.loginerror.hours' => 'Saat(ler)',
    'dochat.loginerror.days' => 'Gün(ler)',
    'dochat.options.textsize' => 'Yazı büyüklüğü',
    'dochat.options.smoothanimations' => 'Transparan animasyon',
    'dochat.options.joinpartmessages' => 'Oda haberleri',
    'dochat.chat.joinRoom' => 'Şu an %ROOM odasında konuşuyorsun',
    'dochat.help.create' => 'bir oda kur',
    'dochat.help.close' => 'bir odayı kapat',
    'dochat.help.invite' => 'bir üyeyi güncel odaya davet et',
    'dochat.help.ignore' => 'oyuncuyu görmemezlikten geliyor',
    'dochat.help.allow' => 'görmemezden geldiğin üyeye, konuşma izni ver',
    'dochat.help.users' => 'bütün odadaki kullanıcıların listesini çıkar',
    'dochat.help.rooms' => 'var olan bütün odaların listesini göster',
    'dochat.help.whisper' => 'üyele birşeyleri gizli konuş',
    'dochat.chat.wrongVersion' => 'Chat programının son sürümünü kullanmıyorsun. Lütfen tarayıcının önbelleğini sil ve oyunu güncelle.',
    'dochat.chat.administrator' => 'Administratör',
    'dochat.chat.moderatorsInRoom' => 'Moderatörlerin bulunduğu oda:',
    'dochat.help.leave' => 'Odadan çık',
    'dochat.chat.roomnameTooLong' => 'Oda ismi çok uzun.',
    'dochat.chat.cannotLeaveRaum' => 'Bu odayı terk edemezsin.',
    'dochat.chat.system' => 'Sistem',
    'dochat.chat.fastReg' => 'Chatte yazabilmek için, tamamen kayıt yaptırman gerekiyor.',
    'dochat.chat.ttip_reconnect' => 'Sohbet sunucusuna tekrar bağlan. Bu durumda sohbet geçmişin silinecek!',
    'dochat.help.report' => 'Open Report interface.',
    'error_timedout' => 'Bağlantı zaman aşımına uğradı.',
    'error_rightscheck' => 'Bu işlemi gerçekleştirmek için izniniz yok.',
    'error_couldnotloginwithoutcredentials' => 'Oturum açmak için giriş bilgileri gerekir.',
    'error_couldnotloginwithwrongcredentials' => 'Giriş bilgileri yanlış.',
    'error_noserver' => 'Uygun sohbet sunucusu yok.',
    'error_internalerror' => 'İç hata oluştu.',
    'error_parsefailed' => 'Mesaj ayrıştırılamadı.',
    'error_custom' => 'Beklenmeyen bir hata oluştu.',
    'reportview_message_info' => 'Lütfen bildirmek istediğiniz mesajı seçin:',
    'reportview_label_reason' => 'Sebep:',
    'reason_spam' => 'Spam',
    'reason_spam_details' => '<b><u>SPAM şudur:</u></b><ul><li>sohbeti rastgele mesajlarla doldurmak</li><li>aynı mesajı defalarca tekrarlamak</li></ul><br><br><b><u>SPAM şu DEĞİLDİR:</u></b><ul><li>sohbeti çok sık kullanmak</li></ul>',
    'reason_bad_name' => 'Sakıncalı oyuncu adı veya klan adı',
    'reason_bad_name_details' => '<b><u>Sakıncalı oyuncu adı veya klan adı şudur:</u></b><ul><li>Küfürlü veya hakaret içeren oyuncu/grup/klan adı</li><br><br></ul><b><u>Sakıncalı oyuncu adı veya klan adı şu DEĞİLDİR:</u></b><ul><li>Özel karakter, yabancı dil veya ascii resim kullanımı</li></ul>',
    'reason_abusive_chat' => 'Kötü Niyetli Sohbet',
    'reason_abusive_chat_details' => '<b><u>Kötü Niyetli Sohbet şudur:</u></b><ul><li>Sohbeti reklam yapmak için kullanmak; açık saçık mesajlar; taciz; ırkçı, politik ya da dinsel cepheleşmeler</li></ul><br><br><b><u>Kötü Niyetli Sohbet şu DEĞİLDİR:</u></b><ul><li>Şaka yapmak, özel konulardan konuşmak, spam, davranışınızı değiştirmeniz için yapılan öneriler</li></ul>',
    'chat_confirm_report' => '%REASON% nedeniyle %USERNAME% için bildirimde bulunmak istiyor musun?',
    'chat_confirm_statement' => 'Geri bildirimin için teşekkürler! Mesajını aldık, topluluk yöneticilerimiz bu konuyla ilgilenecek. Bu sorun için özür dileriz.',
    'achat_ban_user' => '%username% adlı kullanıcı, sistem tarafından engellendi.',
];
