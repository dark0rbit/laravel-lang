<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'IGÉNYLÉS',
    'btn_label_loading' => 'BETÖLTÉS',
    'btn_label_loadstarsystem' => 'Csillagrendszer betöltése',
    'login_bonus_panel_title' => 'NAPI BEJELENTKEZÉSI BÓNUSZ',
    'login_bonus_item_headline' => '%VALUE%. nap',
    'login_bonus_button_get_premium' => 'Légy PRÉMIUM játékos',
    'login_bonus_item_headline_premium' => 'A PRÉMIUM birtokában ezt is megkapod:',
    'login_bonus_item_premium_buybutton' => 'Vásárolj PRÉMIUM tagságot most',
    'login_bonus_calendar_expires' => 'A naptár ennyi idő múlva újraindul: %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'A 25. napi prémium jutalmat csak akkor szerezheted meg, ha az átlagos 25. napi jutalom átvételekor MÁR rendelkezel aktív PRÉMIUM tagsággal!',
    'login_bonus_tooltip_info_items' => 'Vedd át a napi jutalmadat! Semmi probléma! Ha kihagysz egy napot, az előrehaladásod akkor is megmarad, amíg a naptár újra nem indul.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'A jutalom átvételének előfeltétele az aktív PRÉMIUM tagság.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'n',
    'login_time_hour_short' => 'ó',
    'login_time_minute_short' => 'p',
];
