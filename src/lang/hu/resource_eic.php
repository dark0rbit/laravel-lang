<?php

return [
    'equipment_header_device_selector_ship' => 'Űrhajó',
    'ttip_equipment_tab_ship' => 'Nézd meg az űrhajódat és a felszerelését a kiválasztott hangárban!',
    'equipment_header_device_selector_pet' => 'P.E.T.',
    'ttip_equipment_tab_pet' => 'Nézd meg a P.E.T.-edet és a felszerelését a kiválasztott hangárban!',
    'equipment_header_device_selector_drones' => 'Drónok',
    'ttip_equipment_tab_drones' => 'Nézd meg a drónjaidat és a felszerelésüket a kiválasztott hangárban!',
    'equipment_header_configswitch_config1.tooltip' => 'Switch to configuration 1',
    'equipment_header_configswitch_config2.tooltip' => 'Switch to configuration 2',
    'equipment_message_equipped_items_stronger' => 'A jelenleg felszerelt tárgyaid jobbak! Biztosan le akarod cserélni őket?',
    'equipment_equipmentarea_label_lasers' => 'Lézerek',
    'equipment_equipmentarea_label_rocketlaunchers' => 'Rakétavetők',
    'equipment_equipmentarea_label_extras' => 'Extrák',
    'equipment_equipmentarea_label_generators' => 'Generátorok',
    'equipment_equipmentarea_label_gears' => 'Berendezések',
    'equipment_equipmentarea_label_aiprotocols' => 'MI protokollok',
    'equipment_devicedisplay_button_statistics' => 'Statisztikák',
    'equipment_devicedisplay_button_newships' => 'Új űrhajók',
    'equipment_devicedisplay_button_closenewships' => 'Új űrhajók bezárása',
    'equipment_devicedisplay_footer_button_warpin' => 'Hajócsere',
    'equipment_devicedisplay_footer_button_activate' => 'Aktiválás',
    'equipment_devicedisplay_footer_button_allocate' => 'Elhelyezés',
    'equipment_devicedisplay_footer_button_rename_pet' => 'Átnevezés',
    'equipment_inventory_label_inventory' => 'Eszköztár',
    'equipment_inventory_popup.filtering_label_filter' => 'Szűrő',
    'equipment_inventory_popup.filtering_label_showall' => 'Összes mutatása',
    'equipment_inventory_popup.filtering_label_ammunition' => 'Lőszer',
    'equipment_inventory_popup.filtering_label_weapons' => 'Fegyverek',
    'equipment_inventory_popup.filtering_label_generators' => 'Generátorok',
    'equipment_inventory_popup.filtering_label_extras' => 'Extrák',
    'equipment_inventory_popup.filtering_label_pet.gear' => 'P.E.T. berendezések',
    'equipment_inventory_popup.filtering_label_pet.protocols' => 'P.E.T. protokollok',
    'equipment_inventory_popup.filtering_label_modules' => 'Harci bázis',
    'equipment_inventory_popup.filtering_label_drones' => 'Drónok',
    'equipment_inventory_popup.filtering_label_resources' => 'Nyersanyagok',
    'equipment_inventory_popup.filtering_label_item' => 'Különlegességek',
    'equipment_inventory_popup.filtering_label_pet' => 'P.E.T.',
    'equipment_inventory_popup.filtering_label_boosters' => 'Boosterek',
    'equipment_inventory_popup.filtering_label_ships_and_designs' => 'Hajók & designok',
    'equipment_inventory_popup.filtering_label_hardware' => 'Hardware',
    'equipment_inventory_item.tooltip.name' => 'Név',
    'equipment_inventory_item.tooltip.type' => 'Típus',
    'equipment_inventory_item.tooltip.level' => 'Szint',
    'equipment_inventory_item.tooltip.description' => 'Leírás',
    'equipment_inventory_item.tooltip.quantity' => 'Mennyiség',
    'equipment_inventory_item.tooltip.capacity' => 'Kapacitás',
    'equipment_inventory_item.tooltip.absorption' => 'Elnyelés',
    'equipment_inventory_item.tooltip.cooldown' => 'Újratöltési idő',
    'equipment_inventory_item.tooltip.durability' => 'Tartósság',
    'equipment_inventory_item.tooltip.damage' => 'Sebzés',
    'equipment_inventory_item.tooltip.damagebonus' => 'Sebzésbónusz',
    'equipment_inventory_item.tooltip.shieldsteal' => 'Pajzselszívás',
    'equipment_inventory_item.tooltip.installedOn' => 'Beépítés helye:',
    'equipment_inventory_item.tooltip.range' => 'Hatótávolság',
    'equipment_inventory_item.itemcontext.sell' => 'Eladás',
    'equipment_inventory_item.itemcontext.quickbuy' => 'Gyorsvásárlás',
    'equipment_inventory_item.itemcontext.repair' => 'Javítás',
    'equipment_inventory_item.itemcontext.upgrade' => 'Fejlesztés',
    'equipment_inventory_item.itemcontext.configure' => 'Configure',
    'equipment_drone_label_level' => 'Szint: %NUMBER%',
    'equipment_drone_label_upgradelevel' => 'Upgrade Level: %NUMBER%',
    'equipment_drone_label_damage' => 'Sebzés: %NUMBER%%',
    'equipment_popup.confirmation.ok' => 'OK',
    'equipment_popup.confirmation.cancel' => 'Mégsem',
    'equipment.input.invalidate.toolong' => 'A név túl hosszú.',
    'equipment.input.invalidate.tooshort' => 'A név túl rövid.',
    'equipment.input.invalidate.wrongchars' => 'Érvénytelen karaktereket tartalmaz.',
    'equipment_popup.addhangar.title' => 'Hangár hozzáadása',
    'equipment_popup.addhangar.tooltip' => 'Vásárolj még egy hangárt!',
    'equipment_popup.addhangar.message' => 'A hangárban tárolhatod az űrhajóidat, az űrhajócserével pedig könnyedén válthatsz egyikről a másikra, ha aktiválsz egy másik hangárt. Vásárolsz egy hangárt %hangar_price% összegért?',
    'equipment_popup.addhangar.changedprice.message' => 'Ennek a tárgynak az ára megváltozott. %hangar_price% összegért is szeretnéd megvenni a hangárt?',
    'equipment_popup.addhangar.denied.message' => 'A hangárban tárolhatod az űrhajóidat, az űrhajócserével pedig könnyedén válthatsz egyikről a másikra, ha aktiválsz egy másik hangárt. Sajnos most nem áll rendelkezésedre %hangar_price%, hogy vásárolhass egyet.',
    'equipment_popup.addhangar.confirm.buy' => 'Vásárlás',
    'msg_addhangar_success' => 'Sikeres hangárvásárlás.',
    'equipment_popup.allocateship.title' => 'Hajó elhelyezése',
    'equipment_popup.allocateship.tooltip' => 'Helyezd el ezt a hajót a kiválasztott hangárban az "Elhelyezés" gombra kattintva!',
    'equipment_popup.allocateship.message' => 'Helyezz el egy új hajót a kiválasztott hangárban! Figyelem! Ha a hangárban már van hajó, az automatikusan eladásra kerül!',
    'equipment_popup.allocateship.confirm.allocate' => 'Elhelyezés',
    'equipment_popup.clearconfig.title' => 'Konfiguráció törlése',
    'equipment_popup.clearconfig.tooltip' => 'Törli a jelenlegi konfigurációt.',
    'equipment_popup.clearconfig.message' => 'Figyelem! Ez a művelet törli a hajód jelenlegi konfigurációját. Biztosan folytatni szeretnéd?',
    'equipment_popup.clearconfig.confirm.delete' => 'Törlés',
    'equipment_popup.renamehangar.title' => 'Hangár átnevezése',
    'equipment_popup.renamehangar.message' => 'Nevezd át a hangárodat tetszés szerint! (Ezt csak akkor teheted meg, ha aktív prémium tagságod van.)',
    'equipment_popup.renamepet.title' => 'P.E.T. átnevezése',
    'equipment_popup.renamepet.message' => '%pet_rename_price% összegért átnevezheted a P.E.T.-edet.',
    'equipment_popup.itemupgrade.title' => 'Fejlesztés',
    'equipment_popup.itemupgrade.upgrade.message' => 'Fejleszd %level%. szintre!',
    'equipment_popup.itemupgrade.upgrade.chanceofsuccess' => 'Siker esélye:',
    'equipment_popup.itemupgrade.upgrade.totalcosts' => 'Teljes költség:',
    'equipment_popup.itemupgrade.upgrade.confirm' => 'Fejlesztés',
    'equipment_popup.itemupgrade.failure.title' => 'SIKERTELEN',
    'equipment_popup.itemupgrade.failure.message' => 'A fejlesztési kísérlet nem sikerült.',
    'equipment_popup.itemupgrade.success.title' => 'SIKER',
    'equipment_popup.itemupgrade.success.message' => 'Sikeres fejlesztés %level%. szintre!',
    'equipment_popup.itemupgrade.upgrade.damage' => 'Sebzésbónusz fejlesztése, cél: %amount%%.',
    'equipment_popup.itemupgrade.upgrade.shield' => 'Pajzsbónusz fejlesztése, cél: %amount%%.',
    'equipment_popup.itemupgrade.upgrade.durability' => 'Tartóssági bónusz fejlesztése, cél: %amount%.',
    'equipment_popup.itemupgrade.upgrade.booster' => 'Boosterbónusz fejlesztése, cél: %amount%.',
    'equipment_popup.itemupgrade.upgrade.repair' => 'Javítási bónusz fejlesztése, cél: %amount%.',
    'equipment_popup.itemupgrade.upgrade.deflectorminutes' => 'Deflektorpajzs-bónusz fejlesztése, cél: %amount% perc.',
    'equipment_popup.itemupgrade.upgraded.damage' => 'Sebzésbónusz fejlesztve: %amount%%.',
    'equipment_popup.itemupgrade.upgraded.shield' => 'Pajzsbónusz fejlesztve: %amount%%.',
    'equipment_popup.itemupgrade.upgraded.durability' => 'Tartóssági bónusz fejlesztve: %amount%.',
    'equipment_popup.itemupgrade.upgraded.booster' => 'Boosterbónusz fejlesztve: %amount%.',
    'equipment_popup.itemupgrade.upgraded.repair' => 'Javítási bónusz fejlesztve: %amount%.',
    'equipment_popup.itemupgrade.upgraded.deflectorminutes' => 'Deflektorpajzs-bónusz fejlesztve: %amount% perc.',
    'equipment_popup.repair.title' => 'Javítás',
    'equipment_popup.repair.message.drone.price' => 'Szeretnéd megjavítani ezt a drónt %price% %currency%ért?',
    'equipment_popup.repair.message.drone.levelloss' => 'A javítás után %level.from%. szintű helyett csak %level.to%. szintű lesz.',
    'equipment_popup.repair.message.ship' => 'Javítsd meg a hajódat %price% %currency%ért, aztán mehet minden tovább!',
    'equipment_popup.repair.message.item' => 'Megjavítod a kiválasztott tárgyat %price% %currency%ért?',
    'equipment_popup.repair.confirm' => 'Javítás',
    'equipment_popup.configure.title' => 'Configure',
    'equipment_popup.sell.title' => 'Eladás',
    'equipment_popup.sell.message.stackable' => 'Mennyit szeretnél eladni?',
    'equipment_popup.sell.message.unstackable' => 'Biztosan el akarod adni ezt a tárgyat %price% %currency%ért?',
    'equipment_popup.sell.item.maxitems' => 'Összeg',
    'equipment_popup.sell.item.sell' => 'Eladás ennyiért:',
    'equipment_popup.sell.button.max' => 'Max.',
    'equipment_popup.sell.confirm' => 'Eladás',
    'equipment_popup.hangarchange.title' => 'Hangár aktiválása',
    'equipment_popup.hangarchange.message' => 'Válts át a hajóra és a jelenlegi helyére a kiválasztott hangárban %shipwarp_price%ért!',
    'equipment_popup.shipwarp.title' => 'Űrhajócsere',
    'equipment_popup.shipwarp.message' => 'Hozd át a hajót a kiválasztott hangárból a jelenlegi pozíciódra, és válts át rá %shipwarp_price%ért!',
    'desc_hangarchange_costs_spacestation' => 'Ez a hangáraktiválás most ingyenes, mert a megbízód állomásán tartózkodsz.',
    'equipment_popup.buypetslot.title' => 'Üres P.E.T.-hely vásárlása',
    'equipment_popup.buypetslot.message' => 'Biztosan szeretnél P.E.T.-helyet vásárolni %buy_price%ért?',
    'equipment_popup.buypetslot.confirm' => 'Vásárlás',
    'equipment_device_statistic_label_no_pet' => 'Nincs P.E.T.-ed.',
    'equipment_device_statistic_label_no_drones' => 'Jelenleg nincs drónod.',
    'equipment_device_statistic_hull' => 'Hajótest',
    'equipment_device_statistic_nanohull' => 'Nanoburkolat',
    'equipment_device_statistic_laser_dmg' => 'Lézersebzés',
    'equipment_device_statistic_speed' => 'Sebesség',
    'equipment_device_statistic_shield_capacity' => 'Pajzskapacitás',
    'equipment_device_statistic_shield_hull' => 'Pajzs/burkolat sebzésmegoszlás',
    'equipment_device_statistic_pet_name' => 'Név',
    'equipment_device_statistic_pet_fuel' => 'Üzemanyag',
    'equipment_device_statistic_pet_lvl' => 'Szint',
    'equipment_device_statistic_pet_xp' => 'P.E.T. TP',
    'equipment_device_statistic_pet_hp' => 'Életerőpont',
    'equipment_device_statistic_pet_laserdmg' => 'Lézersebzés',
    'equipment_device_statistic_pet_shield_capacity' => 'Pajzskapacitás',
    'equipment_device_statistic_pet_damage_distribution' => 'Pajzs/burkolat sebzésmegoszlás',
    'equipment_device_statistic_drone_lvl' => 'Szint',
    'equipment_device_statistic_drone_damage' => 'Kapott sebzés',
    'equipment_device_statistic_drone_buff' => 'Sebzés-/pajzsbónusz',
    'equipment_device_statistic_drone_xp' => 'Drón-TP',
    'equipment_device_statistic_drone_next_lvl_xp' => 'TP a következő szintig',
    'msg_sastarted' => 'Új hajó aktiválása',
    'equipment_popup.error.title' => 'Hiba',
    'NO_SUCH_SHIP' => 'A keresett hajó nem található.',
    'NO_SUCH_HANGAR' => 'A keresett hangár nem található.',
    'USER_HAS_NO_PET' => 'Nincs P.E.T.-ed.',
    'INCORRECT_PRICE' => 'HIBA!!!\nTÉVES_ÁR.',
    'NOT_ENOUGH_MONEY' => 'HIBA!!!\nNINCS_ELÉG_PÉNZ.',
    'INCORRECT_SLOT_INDEX' => 'HIBA!!!\nTÉVES_HELY_INDEX.',
    'NO_SUCH_DRONE' => 'A keresett drón nem található.',
    'WRONG_SHIP_ID_OR_CONFIG' => 'HIBA!!!\nTÉVES_HAJÓAZONOSÍTÓ_VAGY_KONFIGURÁCIÓ.',
    'TOO_MANY_HANGARS' => 'HIBA!!!\nTÚL_SOK_HANGÁR.',
    'NOT_PREMIUM_USER' => 'HIBA!!!\nNEM_PRÉMIUM_FELHASZNÁLÓ.',
    'INVALID_NAME' => 'HIBA!!!\nÉRVÉNYTELEN_NÉV.',
    'NO_SUCH_DESIGN' => 'A keresett design nem található.',
    'SOME_ITEMS_WERE_NOT_EQUIPPED' => 'HIBA!!!\nEGYES_TÁRGYAK_NINCSENEK_FELSZERELVE.',
    'ILLEGAL_TARGET_SHIPACTIVATION' => 'ERROR !!!\\nThis target can\'t be activated.',
];
