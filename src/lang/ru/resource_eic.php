<?php

return [
    'equipment_header_device_selector_ship' => 'Корабль',
    'ttip_equipment_tab_ship' => 'Просматривай корабль и его вооружение в выбранном ангаре.',
    'equipment_header_device_selector_pet' => 'P.E.T.',
    'ttip_equipment_tab_pet' => 'Просматривай P.E.T. и его вооружение в выбранном ангаре.',
    'equipment_header_device_selector_drones' => 'Дроиды',
    'ttip_equipment_tab_drones' => 'Просматривай дроиды и их вооружение в выбранном ангаре.',
    'equipment_header_configswitch_config1.tooltip' => 'Switch to configuration 1',
    'equipment_header_configswitch_config2.tooltip' => 'Switch to configuration 2',
    'equipment_message_equipped_items_stronger' => 'Действительно перейти к более старой версии? Сейчас твоё вооружение намного лучше!',
    'equipment_equipmentarea_label_lasers' => 'Лазеры',
    'equipment_equipmentarea_label_rocketlaunchers' => 'Ракетомёты',
    'equipment_equipmentarea_label_extras' => 'Дополнения',
    'equipment_equipmentarea_label_generators' => 'Генераторы',
    'equipment_equipmentarea_label_gears' => 'Механизмы',
    'equipment_equipmentarea_label_aiprotocols' => 'Протоколы ИИ',
    'equipment_devicedisplay_button_statistics' => 'Статистика',
    'equipment_devicedisplay_button_newships' => 'Новые корабли',
    'equipment_devicedisplay_button_closenewships' => 'Закрыть новые корабли',
    'equipment_devicedisplay_footer_button_warpin' => 'ССД',
    'equipment_devicedisplay_footer_button_activate' => 'Активировать',
    'equipment_devicedisplay_footer_button_allocate' => 'Переместить',
    'equipment_devicedisplay_footer_button_rename_pet' => 'Изменить название',
    'equipment_inventory_label_inventory' => 'Инвентарь',
    'equipment_inventory_popup.filtering_label_filter' => 'Фильтр',
    'equipment_inventory_popup.filtering_label_showall' => 'Показать все',
    'equipment_inventory_popup.filtering_label_ammunition' => 'Боеприпасы',
    'equipment_inventory_popup.filtering_label_weapons' => 'Оружие',
    'equipment_inventory_popup.filtering_label_generators' => 'Генераторы',
    'equipment_inventory_popup.filtering_label_extras' => 'Дополнения',
    'equipment_inventory_popup.filtering_label_pet.gear' => 'Механизмы P.E.T.',
    'equipment_inventory_popup.filtering_label_pet.protocols' => 'Протоколы P.E.T.',
    'equipment_inventory_popup.filtering_label_modules' => 'Станция',
    'equipment_inventory_popup.filtering_label_drones' => 'Дроиды',
    'equipment_inventory_popup.filtering_label_resources' => 'Сырьё',
    'equipment_inventory_popup.filtering_label_item' => 'Специальные',
    'equipment_inventory_popup.filtering_label_pet' => 'P.E.T.',
    'equipment_inventory_popup.filtering_label_boosters' => 'Бустеры',
    'equipment_inventory_popup.filtering_label_ships_and_designs' => 'Корабли и дизайны',
    'equipment_inventory_popup.filtering_label_hardware' => 'Hardware',
    'equipment_inventory_item.tooltip.name' => 'Название',
    'equipment_inventory_item.tooltip.type' => 'Тип',
    'equipment_inventory_item.tooltip.level' => 'Уровень',
    'equipment_inventory_item.tooltip.description' => 'Описание',
    'equipment_inventory_item.tooltip.quantity' => 'Кол-во',
    'equipment_inventory_item.tooltip.capacity' => 'Мощность',
    'equipment_inventory_item.tooltip.absorption' => 'Поглощение урона',
    'equipment_inventory_item.tooltip.cooldown' => 'Перезарядка',
    'equipment_inventory_item.tooltip.durability' => 'Прочность',
    'equipment_inventory_item.tooltip.damage' => 'Урон',
    'equipment_inventory_item.tooltip.damagebonus' => 'Бонус к урону',
    'equipment_inventory_item.tooltip.shieldsteal' => 'Кража щита',
    'equipment_inventory_item.tooltip.installedOn' => 'Установлено на',
    'equipment_inventory_item.tooltip.range' => 'Дальность',
    'equipment_inventory_item.itemcontext.sell' => 'Продать',
    'equipment_inventory_item.itemcontext.quickbuy' => 'Быстрая покупка',
    'equipment_inventory_item.itemcontext.repair' => 'Ремонт',
    'equipment_inventory_item.itemcontext.upgrade' => 'Улучшение',
    'equipment_inventory_item.itemcontext.configure' => 'Configure',
    'equipment_drone_label_level' => 'Уровень: %NUMBER%',
    'equipment_drone_label_upgradelevel' => 'Upgrade Level: %NUMBER%',
    'equipment_drone_label_damage' => 'Урон: %NUMBER%%',
    'equipment_popup.confirmation.ok' => 'OК',
    'equipment_popup.confirmation.cancel' => 'Отмена',
    'equipment.input.invalidate.toolong' => 'Слишком длинное название.',
    'equipment.input.invalidate.tooshort' => 'Слишком короткое название.',
    'equipment.input.invalidate.wrongchars' => 'Содержит недопустимые символы.',
    'equipment_popup.addhangar.title' => 'Добавить ангар',
    'equipment_popup.addhangar.tooltip' => 'Купить дополнительный ангар.',
    'equipment_popup.addhangar.message' => 'В ангаре можно хранить корабли и быстро переключаться между ними с помощью ССД-процесса или активации другого ангара. Купить ангар за %hangar_price%?',
    'equipment_popup.addhangar.changedprice.message' => 'Кажется, стоимость данного предмета изменилась. Ты всё ещё хочешь купить ангар за %hangar_price%?',
    'equipment_popup.addhangar.denied.message' => 'В ангаре можно хранить корабли и быстро переключаться между ними с помощью ССД-процесса или активации другого ангара. К сожалению, у тебя нет необходимых средств (%hangar_price%) для покупки ангара.',
    'equipment_popup.addhangar.confirm.buy' => 'Купить',
    'msg_addhangar_success' => 'Ангар куплен.',
    'equipment_popup.allocateship.title' => 'Переместить корабль',
    'equipment_popup.allocateship.tooltip' => 'Нажми «Переместить», чтобы поместить этот корабль в выбранный ангар.',
    'equipment_popup.allocateship.message' => 'Перемести новый корабль в выбранный ангар. Внимание! Корабль, находящийся в этом ангаре на данный момент, будет продан атоматически!',
    'equipment_popup.allocateship.confirm.allocate' => 'Переместить',
    'equipment_popup.clearconfig.title' => 'Удалить конфигурацию',
    'equipment_popup.clearconfig.tooltip' => 'Удаляет текущую конфигурацию.',
    'equipment_popup.clearconfig.message' => 'Внимание! Текущая конфигурация корабля будет удалена. Продолжить?',
    'equipment_popup.clearconfig.confirm.delete' => 'Удалить',
    'equipment_popup.renamehangar.title' => 'Переименовать ангар',
    'equipment_popup.renamehangar.message' => 'Меняй название ангара по желанию. (Эта функция доступна только при наличии активной премиум-подписки.)',
    'equipment_popup.renamepet.title' => 'Изменить название P.E.T.',
    'equipment_popup.renamepet.message' => 'Изменить название P.E.T. можно за %pet_rename_price%.',
    'equipment_popup.itemupgrade.title' => 'Улучшение',
    'equipment_popup.itemupgrade.upgrade.message' => 'Улучшить до уровня %level%!',
    'equipment_popup.itemupgrade.upgrade.chanceofsuccess' => 'Шанс на успех:',
    'equipment_popup.itemupgrade.upgrade.totalcosts' => 'Стоимость:',
    'equipment_popup.itemupgrade.upgrade.confirm' => 'Улучшение',
    'equipment_popup.itemupgrade.failure.title' => 'СБОЙ',
    'equipment_popup.itemupgrade.failure.message' => 'Не удалось выполнить улучшение.',
    'equipment_popup.itemupgrade.success.title' => 'УСПЕХ',
    'equipment_popup.itemupgrade.success.message' => 'Улучшено до уровня %level%!',
    'equipment_popup.itemupgrade.upgrade.damage' => 'Улучшить бонус к урону до %amount%%.',
    'equipment_popup.itemupgrade.upgrade.shield' => 'Улучшить бонус щита до %amount%%.',
    'equipment_popup.itemupgrade.upgrade.durability' => 'Улучшить бонус к прочности до %amount%.',
    'equipment_popup.itemupgrade.upgrade.booster' => 'Улучшить бонус бустера до %amount%.',
    'equipment_popup.itemupgrade.upgrade.repair' => 'Улучшить бонус ремонта до %amount%.',
    'equipment_popup.itemupgrade.upgrade.deflectorminutes' => 'Улучшить дефлекторный бонус до %amount%.',
    'equipment_popup.itemupgrade.upgraded.damage' => 'Бонус к урону улучшен до %amount%%.',
    'equipment_popup.itemupgrade.upgraded.shield' => 'Бонус щита улучшен до %amount%%.',
    'equipment_popup.itemupgrade.upgraded.durability' => 'Бонус к прочности улучшен до %amount%.',
    'equipment_popup.itemupgrade.upgraded.booster' => 'Бонус бустера улучшен до %amount%.',
    'equipment_popup.itemupgrade.upgraded.repair' => 'Бонус ремонта улучшен до %amount%.',
    'equipment_popup.itemupgrade.upgraded.deflectorminutes' => 'Дефлекторный бонус улучшен до %amount%.',
    'equipment_popup.repair.title' => 'Ремонт',
    'equipment_popup.repair.message.drone.price' => 'Отремонтировать этот дроид за %price% %currency%?',
    'equipment_popup.repair.message.drone.levelloss' => 'Его уровень упадёт с %level.from% до %level.to%.',
    'equipment_popup.repair.message.ship' => 'Отремонтируй корабль за %price% %currency% и готовься к следующему раунду!',
    'equipment_popup.repair.message.item' => 'Отремонтировать выбранный предмет за %price% %currency%?',
    'equipment_popup.repair.confirm' => 'Ремонт',
    'equipment_popup.configure.title' => 'Configure',
    'equipment_popup.sell.title' => 'Продать',
    'equipment_popup.sell.message.stackable' => 'Сколько ты хочешь продать?',
    'equipment_popup.sell.message.unstackable' => 'Действительно продать этот предмет за %price% %currency%?',
    'equipment_popup.sell.item.maxitems' => 'Кол-во',
    'equipment_popup.sell.item.sell' => 'Продать за',
    'equipment_popup.sell.button.max' => 'Max.',
    'equipment_popup.sell.confirm' => 'Продать',
    'equipment_popup.hangarchange.title' => 'Активировать ангар',
    'equipment_popup.hangarchange.message' => 'Переключись на корабль и его текущую позицию в выбранном ангаре за %shipwarp_price%.',
    'equipment_popup.shipwarp.title' => 'ССД корабля',
    'equipment_popup.shipwarp.message' => 'Используй ССД-технологию, чтобы телепортировать корабль в выбранном ангаре к твоей текущей позиции, и переключись на него за %shipwarp_price%.',
    'desc_hangarchange_costs_spacestation' => 'Активация ангара бесплатна, так как ты находишься на станции компании.',
    'equipment_popup.buypetslot.title' => 'Купить слот для P.E.T.',
    'equipment_popup.buypetslot.message' => 'Действительно купить слот для P.E.T. за %buy_price%?',
    'equipment_popup.buypetslot.confirm' => 'Купить',
    'equipment_device_statistic_label_no_pet' => 'У тебя нет P.E.T.',
    'equipment_device_statistic_label_no_drones' => 'На данный момент у тебя нет дроидов.',
    'equipment_device_statistic_hull' => 'Корпус',
    'equipment_device_statistic_nanohull' => 'Нанообшивка',
    'equipment_device_statistic_laser_dmg' => 'Урон от лазеров',
    'equipment_device_statistic_speed' => 'Скорость',
    'equipment_device_statistic_shield_capacity' => 'Мощность щита',
    'equipment_device_statistic_shield_hull' => 'Распределение урона м/у щитом и обшивкой',
    'equipment_device_statistic_pet_name' => 'Название',
    'equipment_device_statistic_pet_fuel' => 'Топливо',
    'equipment_device_statistic_pet_lvl' => 'Уровень',
    'equipment_device_statistic_pet_xp' => 'ОП P.E.T.',
    'equipment_device_statistic_pet_hp' => 'Здоровье',
    'equipment_device_statistic_pet_laserdmg' => 'Урон от лазеров',
    'equipment_device_statistic_pet_shield_capacity' => 'Мощность щита',
    'equipment_device_statistic_pet_damage_distribution' => 'Распределение урона м/у щитом и обшивкой',
    'equipment_device_statistic_drone_lvl' => 'Уровень',
    'equipment_device_statistic_drone_damage' => 'Полученный урон',
    'equipment_device_statistic_drone_buff' => 'Бонус урона/щита',
    'equipment_device_statistic_drone_xp' => 'ОП дроида',
    'equipment_device_statistic_drone_next_lvl_xp' => 'ОП для след. уровня',
    'msg_sastarted' => 'Новый корабль активируется',
    'equipment_popup.error.title' => 'Ошибка',
    'NO_SUCH_SHIP' => 'Запрошенный корабль не найден.',
    'NO_SUCH_HANGAR' => 'Запрошенный ангар не найден.',
    'USER_HAS_NO_PET' => 'У тебя нет P.E.T.',
    'INCORRECT_PRICE' => 'ОШИБКА!!!\nНЕПРАВИЛЬНАЯ ЦЕНА.',
    'NOT_ENOUGH_MONEY' => 'ОШИБКА!!!\nНЕ ХВАТАЕТ СРЕДСТВ.',
    'INCORRECT_SLOT_INDEX' => 'ОШИБКА!!!\nНЕПРАВИЛЬНЫЙ ИНДЕКС СЛОТА.',
    'NO_SUCH_DRONE' => 'Запрашиваемый дроид не найден.',
    'WRONG_SHIP_ID_OR_CONFIG' => 'ОШИБКА!!!\nНЕПРАВИЛЬНОЕ ID ИЛИ КОНФИГУРАЦИЯ КОРАБЛЯ.',
    'TOO_MANY_HANGARS' => 'ОШИБКА!!!\nСЛИШКОМ МНОГО АНГАРОВ.',
    'NOT_PREMIUM_USER' => 'ОШИБКА!!!\nНЕ ПРЕМИУМ-ПОЛЬЗОВАТЕЛЬ.',
    'INVALID_NAME' => 'ОШИБКА!!!\nНЕДЕЙСТВИТЕЛЬНОЕ НАЗВАНИЕ.',
    'NO_SUCH_DESIGN' => 'Запрошенный дизайн не найден.',
    'SOME_ITEMS_WERE_NOT_EQUIPPED' => 'ОШИБКА!!!\nНЕКОТОРЫЕ ПРЕДМЕТЫ НЕ УСТАНОВЛЕНЫ.',
    'ILLEGAL_TARGET_SHIPACTIVATION' => 'ERROR !!!\\nThis target can\'t be activated.',
];
