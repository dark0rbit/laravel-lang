<?php

return [
    'btn_label_start' => 'СТАРТ',
    'btn_label_claim' => 'ВЗЯТЬ',
    'btn_label_loading' => 'ЗАГРУЗКА',
    'btn_label_loadstarsystem' => 'ЗАГРУЗКА ЗВЁЗДНОЙ СИСТЕМЫ',
    'login_bonus_panel_title' => 'ЕЖЕДНЕВНЫЙ БОНУС ЗА ВХОД',
    'login_bonus_item_headline' => 'День %VALUE%',
    'login_bonus_button_get_premium' => 'Получить ПРЕМИУМ',
    'login_bonus_item_headline_premium' => 'ПРЕМИУМ также даёт:',
    'login_bonus_item_premium_buybutton' => 'Купить ПРЕМИУМ',
    'login_bonus_calendar_expires' => 'Сброс календаря через %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Чтобы получить Премиум-бонус на 25-й день входа в игру, у тебя ДОЛЖНА быть действующая ПРЕМИУМ-подписка до принятия стандартной награды на 25-й день!',
    'login_bonus_tooltip_info_items' => 'Получай ежедневную награду. Не переживай! Если даже ты и пропустишь один день, твой прогресс сохранится до сброса календаря.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Чтобы получить награду, у тебя должна быть действующая ПРЕМИУМ-подписка.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'д.',
    'login_time_hour_short' => 'ч.',
    'login_time_minute_short' => 'м.',
];
