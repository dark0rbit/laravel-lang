<?php

return [
    'btn_label_start' => 'DÉPART',
    'btn_label_claim' => 'RÉCUP.',
    'btn_label_loading' => 'CHARGEMENT',
    'btn_label_loadstarsystem' => 'CHARGEMENT DU SYSTÈME STELLAIRE',
    'login_bonus_panel_title' => 'BONUS DE CONNEXION',
    'login_bonus_item_headline' => 'Jour %VALUE%',
    'login_bonus_button_get_premium' => 'Bénéficiez des avantages PREMIUM',
    'login_bonus_item_headline_premium' => 'Avec les avantages PREMIUM, vous recevrez également :',
    'login_bonus_item_premium_buybutton' => 'Passez au PREMIUM maintenant',
    'login_bonus_calendar_expires' => 'Le calendrier sera réinitialisé dans %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Pour recevoir la récompense Premium du 25e jour de connexion, vous DEVEZ posséder un abonnement PREMIUM en cours de validité avant d\'accepter la récompense standard du 25e jour !',
    'login_bonus_tooltip_info_items' => 'Recevez votre récompense quotidienne. Ne vous en faites pas : Si vous oubliez un jour, votre progression mensuelle sera conservée jusqu\'à la réinitialisation du calendrier.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Vous devez posséder un abonnement PREMIUM en cours de validité avant de récupérer votre récompense.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x %VALUE%',
    'login_time_day_short' => 'j',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
