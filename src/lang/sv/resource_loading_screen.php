<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'HÄMTA',
    'btn_label_loading' => 'LADDAR',
    'btn_label_loadstarsystem' => 'LADDA STJÄRNSYSTEM',
    'login_bonus_panel_title' => 'DAGLIG INLOGGNINGSBONUS',
    'login_bonus_item_headline' => 'Dag %VALUE%',
    'login_bonus_button_get_premium' => 'Skaffa PREMIUM',
    'login_bonus_item_headline_premium' => 'Med PREMIUM får du också:',
    'login_bonus_item_premium_buybutton' => 'Köp PREMIUM nu!',
    'login_bonus_calendar_expires' => 'Kalendern nollställs om %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'För att hämta ut Premiumbelöningen på den 25:e dagen så MÅSTE du ha ett aktivt Premiummedlemskap innan du accepterar den vanliga belöningen på den 25:e dagen!',
    'login_bonus_tooltip_info_items' => 'Få din dagliga belöning. Oroa dig inte: Om du missar en dag så förlorar du inte dina framsteg förrän kalendern nollställs.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Du måste ha ett aktivt Premiummedlemskap innan du kan hämta ut belöningen.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE %',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
