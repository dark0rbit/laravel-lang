<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'CLAIM',
    'btn_label_loading' => 'LOADING',
    'btn_label_loadstarsystem' => 'LOAD STAR SYSTEM',
    'login_bonus_panel_title' => 'DAILY LOGIN BONUS',
    'login_bonus_item_headline' => 'Day %VALUE%',
    'login_bonus_button_get_premium' => 'Get PREMIUM',
    'login_bonus_item_headline_premium' => 'With PREMIUM you also get:',
    'login_bonus_item_premium_buybutton' => 'Buy PREMIUM now',
    'login_bonus_calendar_expires' => 'Calendar resets in %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'To claim the Premium 25th day reward you MUST have active PREMIUM Membership before accepting the standard 25th day reward!',
    'login_bonus_tooltip_info_items' => 'Get your daily reward. Don\'t worry: If you miss a day, you won\'t lose your progress until the calendar resets.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'You have to own an active PREMIUM membership before you can claim the reward.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'd',
    'login_time_hour_short' => 'h',
    'login_time_minute_short' => 'm',
];
