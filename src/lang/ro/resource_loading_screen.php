<?php

return [
    'btn_label_start' => 'START',
    'btn_label_claim' => 'CERE',
    'btn_label_loading' => 'LOADING',
    'btn_label_loadstarsystem' => 'ÎNCARCĂ SISTEMUL INTERSTELAR',
    'login_bonus_panel_title' => 'BONUS AUTENTIFICARE ZILNICĂ',
    'login_bonus_item_headline' => 'Ziua %VALUE%',
    'login_bonus_button_get_premium' => 'Pune mâna pe PREMIUM',
    'login_bonus_item_headline_premium' => 'Cu PREMIUM vei primi de asemenea:',
    'login_bonus_item_premium_buybutton' => 'Cumpără PREMIUM acum',
    'login_bonus_calendar_expires' => 'Calendarul se resetează în %VALUE%',
    'login_bonus_tooltip_info_premiumitem' => 'Pentru a revendica recompensa Premium din ziua 25 TREBUIE să fii membru PREMIUM înainte să accepți recompensa standard pentru ziua 25!',
    'login_bonus_tooltip_info_items' => 'Obține recompensa zilnică. Nu te îngrijora: Dacă lipsești o zi nu îți vei pierde progresul cât timp calendarul nu se resetează.',
    'login_bonus_tooltip_info_premiumitem_deactivated' => 'Trebuie să fii membru PREMIUM activ înainte să poți revendica recompensa.',
    'login_booster_time_info' => 'h',
    'login_reward_amount_info' => 'x%VALUE%',
    'login_time_day_short' => 'z',
    'login_time_hour_short' => 'o',
    'login_time_minute_short' => 'm',
];
