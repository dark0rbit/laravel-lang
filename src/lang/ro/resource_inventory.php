<?php

return [
    'thousands_separator' => '.',
    'decimal_separator' => ',',
    'ui_item_expiration' => 'Durata: %VALUE%',
    'label_tab_ship' => 'ASTRONAVĂ',
    'label_tab_drones' => 'DRONE',
    'label_tab_pet' => 'P.E.T.',
    'label_button_generic_abort' => 'STOP',
    'label_button_generic_maximum_amount' => 'MAX.',
    'label_button_split_confirm' => 'ÎMPARTE',
    'label_button_sell' => 'VINDE',
    'label_button_sell_confirm' => 'VINDE',
    'label_button_quick_buy' => 'CUMPĂRĂ ACUM',
    'label_button_repair' => 'REPARĂ',
    'label_button_buy_confirm' => 'CUMPĂRĂ',
    'label_button_clear_config_confirm' => 'Şterge',
    'label_ui_configuration' => 'CONFIGURAŢIE',
    'label_ui_inventory' => 'INVENTAR',
    'label_slotset_lasers' => 'LASER',
    'label_slotset_rockets' => 'RACHETE',
    'label_slotset_extras' => 'EXTRAS',
    'label_slotset_protocols' => 'PROTOCOALE',
    'label_slotset_gears' => 'MODULE',
    'label_slotset_generators' => 'GENERATOARE',
    'label_slotset_visual' => 'PERSONALIZARE',
    'label_slotset_drone_iris' => 'IRIS',
    'label_slotset_drone_flax' => 'FLAX',
    'label_slotset_drone_apis' => 'APIS',
    'label_slotset_drone_zeus' => 'ZEUS',
    'label_slotset_heavy_guns' => 'ROCKET LAUNCHER',
    'label_button_edit_name' => 'REDENUMEŞTE',
    'label_button_use_confirm' => 'CONFIRMĂ',
    'label_button_edit_name_confirm' => 'CONFIRMĂ',
    'label_button_use' => 'UTILIZEAZĂ',
    'label_empty_drone_tab' => 'Nu ai drone! Le poţi cumpăra din Shop.',
    'label_empty_pet_tab' => 'Nu ai P.E.T.! Îl poţi cumpăra din Shop.',
    'label_button_select_ship' => 'Selectează',
    'label_drone_info_level' => 'Nivel: %VALUE%',
    'label_drone_info_effect' => 'Efect: %VALUE%',
    'label_drone_info_damage' => 'Daune: %VALUE%',
    'label_drone_info_design' => 'Design: %VALUE%',
    'label_drone_info_upgrade_level' => 'Niv. upgrade',
    'label_drone_info_damage_level' => 'Daune: %VALUE%',
    'label_drone_info_shield_level' => 'Scut: %VALUE%',
    'label_button_quick_buy_confirm' => 'Cumpără',
    'label_button_upgrade' => 'UPGRADE',
    'label_button_sell_ship_confirm' => 'VINDE',
    'label_button_restart' => 'Restart.',
    'label_dronedesign_desc_visual' => 'În acest loc poți echipa doar designuri vizuale.',
    'label_dronedesign_desc_stats' => 'În acest loc poți echipa designuri funcționale.',
    'tooltip_petdesign_visual_slot' => 'În acest loc poți echipa designuri P.E.T. cosmetice.',
    'tooltip_petdesign_stats_slot' => 'În acest loc poți echipa designuri P.E.T. funcționale.',
    'tooltip_button_repair' => 'Repară itemul selectat.',
    'tooltip_button_buy_confirm' => 'Cumpără acest item în cantitatea selectată.',
    'tooltip_button_clear_config_confirm' => 'Confirmă ştergerea.',
    'tooltip_button_clear_config' => 'Şterge configuraţia actuală.',
    'tooltip_ui_inventory' => 'Gestionează-ţi inventarul aici.',
    'tooltip_tab_ship' => 'Echipament navă',
    'tooltip_tab_drones' => 'Echipament dronă',
    'tooltip_tab_pet' => 'Echipament P.E.T.',
    'tooltip_button_generic_abort' => 'Stop',
    'tooltip_button_generic_maximum_amount' => 'Selectează cantitatea maximă posibilă.',
    'tooltip_button_split_confirm' => 'Împarte stiva în dimensiunile dorite.',
    'tooltip_button_sell' => 'Vinde itemul selectat.',
    'tooltip_button_sell_confirm' => 'Vinde cantitatea selectată.',
    'tooltip_button_quick_buy' => 'Cumpără automat itemul selectat.',
    'tooltip_item_expires' => 'Durata rămasă: %VALUE%',
    'tooltip_button_use_confirm' => 'Confirmă alegerea.',
    'tooltip_button_edit_name' => 'Dă-i P.E.T-ului tău un nou nume.',
    'tooltip_button_edit_name_confirm' => 'Confirmă numele ales.',
    'tooltip_button_use' => 'Utilizează itemul selectat.',
    'tooltip_ship_design_menu' => 'Alege un design de navă.',
    'tooltip_configuration' => 'Alege o configuraţie.',
    'tooltip_filter_menu' => 'Filtrează-ţi inventarul în funcţie de parametrii selectaţi.',
    'tooltip_filter_hangar_menu' => 'Filtrează inventarul în funcţie de hala de hangar selectată.',
    'tooltip_button_open_inventory' => 'Clichează aici pentru a maximiza inventarul.',
    'tooltip_button_close_inventory' => 'Clichează aici pentru a minimiza inventarul.',
    'tooltip_expand_collapse_button' => 'Arată/Ascunde zone de echipament.',
    'tooltip_pet_locked_slot' => 'Clichează pe slot pentru a-l activa.',
    'tooltip_pet_rename_button' => 'Schimbă numele P.E.T-ului tău.',
    'tooltip_button_sell_ship_confirm' => 'Confirmă vânzarea',
    'inventory_item_details_field_name' => 'Nume: %VALUE%',
    'inventory_item_details_field_type' => 'Tip: %VALUE%',
    'inventory_item_details_field_value' => 'Valoare: %VALUE%',
    'inventory_item_details_field_charge' => 'Încărcături: %VALUE%',
    'inventory_item_details_field_description' => 'Descriere: %VALUE%',
    'inventory_item_details_field_durability' => 'Durata: %VALUE%',
    'inventory_item_details_none_selected' => 'Niciun item selectat.',
    'inventory_item_details_field_damage' => 'Nivel daune: %VALUE%',
    'inventory_item_details_field_shield' => 'Nivel scut: %VALUE%',
    'inventory_item_details_field_upgrade' => 'Nivel upgrade: %VALUE%',
    'inventory_item_details_field_current_hitpoints' => 'HP-uri actuale: %VALUE%',
    'inventory_item_details_field_max_hitpoints' => 'HP max.:  %VALUE%',
    'inventory_item_details_field_installed' => 'Instalat: %VALUE%',
    'inventory_item_details_promotion_antec_wordpuzzle' => 'A letter for the Word Puzzle. If the Word is complete you will receive a capsule you can convert into a lottery ticket.',
    'popup_import_config_title' => 'Sistem de transfer',
    'popup_import_config_description' => 'Transferă configurația și echipamentul unei nave către altă navă cu același număr de locuri!',
    'popup_sell_title' => 'VINDE',
    'popup_sell_credits' => 'Credite',
    'popup_split_title' => 'ÎMPARTE STIVA',
    'popup_split_description' => 'Alege aici cât de multe itemuri vrei să muţi în slotul selectat.',
    'popup_quick_buy_title' => 'CUMPĂRĂ ACUM',
    'popup_quick_buy_description' => 'Alege aici cât de multe itemuri vrei să recumperi automat.',
    'popup_title_buy_extra_pet_slot' => 'CUMPĂRĂ SLOT EXTRA',
    'popup_description_buy_extra_pet_slot' => 'Vrei să cumperi un slot suplimentar pentru P.E.T-ul tău?',
    'popup_clear_config_title' => 'Şterge configuraţie',
    'popup_clear_config_description' => 'Atenţie! Această acţiune şterge configuraţia actuală a navei. eşti sigur că vrei să continui?',
    'popup_rocket_auto_buy_cpu_description' => 'Selectează tipul de rachete pe care vrei să-l recumperi.',
    'popup_edit_name_title' => 'Nume P.E.T.',
    'popup_edit_name_description' => 'Alege un nume pentru P.E.T-ul tău. Numele trebuie să fie în conformitate cu CCG.',
    'popup_repair_module_description' => 'Repară modulul selectat.',
    'popup_sell_description' => 'Cât vrei să vinzi?',
    'popup_ammo_auto_buy_cpu_title' => 'ASISTENT MUNIŢII',
    'popup_ammo_auto_buy_cpu_description' => 'Selectează tipul de muniţie pe care vrei s-o cumperi.',
    'popup_rocket_auto_buy_cpu_title' => 'ASISTENT RACHETE',
    'popup_repair_title' => 'REPARĂ',
    'popup_repair_description' => 'Repară itemul selectat.',
    'popup_repair_drone' => 'Repară această dronă cu %AMOUNT% %CURRENCY%.',
    'popup_repair_drone_with_vouchers' => 'Repară această dronă cu un bon.',
    'popup_repair_drone_addendum' => 'Nivelul de la %PREVIOUS_LEVEL%° la %LEVEL%°.',
    'popup_repair_module' => 'Vrei să repari modulul %NAME% cu %AMOUNT% credite?',
    'popup_buy_pet_slot_title' => 'Cumpără slot P.E.T.',
    'popup_buy_pet_slot_description' => 'Cumpără un slot pentru P.E.T. cu %AMOUNT% uridium.',
    'popup_sell_ship_title' => 'VINDE NAVĂ',
    'popup_sell_ship_description' => 'Chiar vrei să vinzi nava (%SHIP_NAME%) pentru %PRICE%?',
    'popup_unspecific_error_description' => 'A apărut o eroare necunoscută. Fă clic pe <i>Restart</i> pentru a reporni pagina de echipament.',
    'pricetag_credits' => '%VALUE% Credite',
    'pricetag_credits_compact' => '%VALUE% C.',
    'pricetag_uridium' => '%VALUE% uridium',
    'pricetag_uridium_compact' => '%VALUE% U.',
    'combobox_all_hangars_name' => 'Toate halele de hangar',
    'combobox_hangar_name' => 'Hală de hangar %HANGARSLOTID%',
    'error_one_item_type' => 'Acest item se foloseşte doar o singură dată pe fiecare configurare.',
    'error_no_free_slot' => 'Nu ai niciun slot liber.',
    'context_buy_ammo_caption' => 'Cumpără automat [ %TYPE% ]',
    'context_buy_rocket_caption' => 'Cumpără automat rachete [ %TYPE% ]',
    'overlay_message_loading' => 'ÎNCARCĂ...',
    'overlay_message_error' => 'EROARE!',
    'filter_user' => 'Utilizator',
    'filter_generators' => 'Generatoare',
    'filter_ammunition' => 'Muniţie',
    'filter_extras' => 'Extras',
    'filter_weapons' => 'Arme',
    'filter_resources' => 'Resurse',
    'filter_modules' => 'Modules',
    'filter_drone_designs' => 'Design-uri drone',
    'filter_drone_related' => 'Resurse drone',
    'filter_pet_related' => 'Despre P.E.T.',
    'label_shipselector_item_renderer_ship_owned' => 'DEȚINUTĂ',
    'label_shipselector_item_renderer_ship_not_owned' => 'NEDEȚINUTĂ',
    'label_shipselector_item_renderer_ship_active' => 'ACTIVĂ',
    'label_shipselector_item_renderer_ship_available' => 'DISPONIBILĂ',
    'label_shipselector_item_renderer_ship_notavailable' => 'NOT AVAILABLE',
    'label_button_ship_selector_name' => 'ÎNAPOI',
    'label_button_manage_equipment_name' => 'GESTIONEAZĂ',
    'label_button_buy_ship_name' => 'CUMPĂRĂ',
    'label_button_activate_ship_name' => 'ACTIVEAZĂ',
    'label_active_ship' => 'ACTIVĂ',
    'label_button_import_load_out' => 'Sistem de transfer',
    'label_button_import' => 'Transfer',
];
